[![Kotlin](https://img.shields.io/badge/kotlin-1.0.0--rc--1036-blue.svg)](http://kotlinlang.org) [![License Apache](https://img.shields.io/badge/License-Apache%202.0-red.svg)](http://www.apache.org/licenses/LICENSE-2.0)

Mashov Unofficial API for JVM platform
======================================

[Mashov](http://www.mashov.info/) is an Israeli system for immediate feedback for teaching institutions, supporting: grades, behave events, timetable, messages and more.

![Project Logo](http://i.imgur.com/JMd2r1l.png?1)

# Maven dependency
**Step 1. Add this repository to your build file**
```groovy
repositories {
	    maven {
	        url "https://dl.bintray.com/yoavst/maven/"
	    }
	}
```
**Step 2. Add the dependency in the form**
```groovy
dependencies {
	        compile 'com.yoavst.mashov:mashov-api:3.2017032'
	}
```


# API
First, create the API controller.
* Kotlin
```kotlin
val apiController = ApiControllerImpl(RequestsControllerImpl, CookieManagerImpl, DataParserImpl)
```

* Java
```java
ApiController apiController = new ApiControllerImpl(RequestsControllerImpl.INSTANCE, CookieManagerImpl.INSTANCE, DataParserImpl.INSTANCE);
```

`ApiControllerImpl` is responsible on creating the requests to the API. It is based on `RequestControllerImpl`
which is responsible on sending the requests as HTTP requests. It is also based on `DataParserImpl` which is 
responsible on parsing the JSON string to objects. `DataParserImpl` currently uses `gson` api for parsing.
`CookieManagerImpl` is responsible on handling `X-Csrf-Token` and other cookies used by Mashov.

## List Of schools
The first step of login is to choose the user's school.

* Kotlin
```kotlin
apiController.getSchools() successNotNull { schools ->
  // Handle schools - List<School>
} fail { exception -> exception.printStackTrace() }
```

* Java
```java
apiController.getSchools().success(new Callback<List<School>>() {
    @Override
    public void process(List<School> schools) {
        // Handle schools
    }
}).fail(new Callback<Exception>() {
    @Override
    void process(Exception exception) {
        exception.printStackTrace();
    }
});
```

*From now I'll show only the Kotlin version, since the Java version is too long. However it should look very close to this example.*

**Note:** If you use Kotlin then the controller return nullable type. use `successNotNull` extension method to work with non-null type.

## Login
After the user chooses school, now it is time to login.

```kotlin
val school = ...
val id = "123456789"
val password = "*******"
val year = 2016
apiController.login(school, id, password, year) successNotNull { login ->
   // Handle login: LoginData for data, Student[] for students.
} error { exception -> // Login failed or problem with network }
```

`LoginData` class contains three important keys:
* `sessionId` - Change every login. Used for authentication.
* `correlationId` - Was useful once, now probably does nothing.
* `userId` - Static for every user. Used in the REST API.

*It is a good idea to save them, since if they are lost, you'll have to login again.*

The declaration of `Student` class is:
```kotlin
data class Student(val id: String, val privateName: String, val familyName: String, val classCode: String, val classNum: Int)
```

## Data
Most the API method takes a `correlationId` and `userId`.   
They are all `GET` requests to `https://svc.mashov.info/api/students/$userId/$api` with `Authorization` header.  

Available APIs yet:
* Picture - `fun getPicture(correlationId: String, userId: String, file: File): Promise<File, Exception>`
* Grades - `fun getGrades(correlationId: String, userId: String): Promise<List<Grade>, Exception>`
* Bagrut Grades - `fun getBagrutGrades(correlationId: String, userId: String): Promise<List<BagrutGrade>, Exception>`
* Behave Events - `fun getBehaveEvents(correlationId: String, userId: String): Promise<List<BehaveEvent>, Exception>`
* Groups - `fun getGroups(correlationId: String, userId: String): Promise<List<Group>, Exception>`
* Lessons count - `fun getLessonsCount(correlationId: String, userId: String): Promise<List<LessonCount>, Exception>`
* Timetable - `fun getTimetable(correlationId: String, userId: String): Promise<List<Lesson>, Exception>`
* Alfon - `fun getAlfon(correlationId: String, userId: String): Promise<List<Contact>, Exception>`
* Inbox - `fun getMessages(correlationId: String): Promise<List<MessageTitle>, Exception>`
* Mail and phone - `fun getDetails(correlationId: String): Promise<UserDetails, Exception>`
* Logout - `fun logout(correlationId: String): Promise<Unit, Exception>`

More will be added soon.

## Work with JSON String
Sometimes you don't need the JSON to be serialized to a POJO. In this case, you can register raw data processor.

* Kotlin
```kotlin
apiController.attachRawDataProcessor { data, api ->
        // Process raw data here. Return true if you want it to be serialized to POJO.
        true
    }
```

* Java
```java
apiController.attachRawDataProcessor(new Function2<String, ApiController.Api, Boolean>() {
            @Override
            public Boolean invoke(String s, ApiController.Api api) {
                // Process raw data here. Return true if you want it to be serialized to POJO.
                return true;
            }
        });
```

**Note:** If you return false, it will pass `null` to `success` callback.

## Save CSRF Token and MashovSessionID

```kotlin

// on restore
CookieControllerImpl.csrfToken = token
CookieControllerImpl.mashovSessionId = sessionId

CookieControllerImpl.attachListener(1) {
    // Save both token and session id to database.
}
```

## Sample responses from Mashov servers
Check out `src/test/resources` for sample responses.

# Technology behind
## Kotlin
Statically typed programming language for the JVM, Android and the browser. Kotlin to Java is like C++ to C.
Kotlin libraries used in this project:
* [Kovenant](http://kovenant.komponents.nl/) - The easy asynchronous library for Kotlin. With extensions for Android, LMAX Disruptor, JavaFX and much more.
* [Fuel](https://github.com/kittinunf/Fuel) - The easiest HTTP networking library in Kotlin for Android. **I'm a contributor there!**