package com.yoavst.mashov;

import kotlin.Unit;
import kotlin.jvm.functions.Function1;

@SuppressWarnings("WeakerAccess")
public abstract class Callback<K> implements Function1<K, Unit> {
    @Override
    public Unit invoke(K value) {
        process(value);
        return Unit.INSTANCE;
    }

    abstract void process(K value);
}
