package com.yoavst.mashov.model

import com.google.gson.annotations.SerializedName

data class Student(@SerializedName("childGuid") val id: String, val familyName: String, val privateName: String, val classCode: String, val classNum: Int)