package com.yoavst.mashov.model


class ApiLevelException(val current: String, val server: String): Exception("Error: Server's Api version is $server while ours is $current")