package com.yoavst.mashov.model

data class Lesson(val groupId: Int, val day: Int, val hour: Int, val subject: String, val teacher: String)