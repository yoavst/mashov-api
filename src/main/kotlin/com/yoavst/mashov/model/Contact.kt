package com.yoavst.mashov.model

data class Contact(val familyName: String, val privateName: String, val classCode: String, val classNum: Int, val city: String,
                          val address: String, val phone: String, val email: String, val cellphone: String)