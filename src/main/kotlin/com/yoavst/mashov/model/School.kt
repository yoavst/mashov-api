package com.yoavst.mashov.model

import com.google.gson.annotations.SerializedName


data class School(@SerializedName("semel") val id: Int,
                         @SerializedName("name") val name: String,
                         @SerializedName("years") val years: IntArray)