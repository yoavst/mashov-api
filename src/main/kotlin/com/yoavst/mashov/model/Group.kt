package com.yoavst.mashov.model

import com.google.gson.annotations.SerializedName

data class Group(val groupId: Int,
                        @SerializedName("subjectName") val subject: String,
                        @SerializedName("teacherName") val teacher: String)