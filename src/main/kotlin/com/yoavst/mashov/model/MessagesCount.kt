package com.yoavst.mashov.model

data class MessagesCount(val allMessages: Int, val inboxMessages: Int, val newMessages: Int, val unreadMessages: Int)