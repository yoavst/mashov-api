package com.yoavst.mashov.model

data class Login(val data: LoginData, val students: List<Student>)