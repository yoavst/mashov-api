package com.yoavst.mashov.model

data class HttpResult<T>(val url: String, val statusCode: Int, val headers: Map<String, List<String>>, val data: T)