package com.yoavst.mashov.model

import com.google.gson.annotations.SerializedName

data class BagrutGrade(val moed: String,
                              @SerializedName("semel") val id: String,
                              val name: String,
                              @SerializedName("shnaty") val yearly: Int,
                              val test: Int,
                              val final: Int)