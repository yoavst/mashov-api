package com.yoavst.mashov.model

data class Grade(val teacher: String, val groupId: Int, val subject: String, val eventDate: Long, val event: String, val type: Int, val grade: Int)