package com.yoavst.mashov.model

data class LoginData(val sessionId: String, val userId: String, val id: String, val userType: Int, val schoolUserType: Int,
                            val schoolId: Int, val year: Int, val correlationId: String)