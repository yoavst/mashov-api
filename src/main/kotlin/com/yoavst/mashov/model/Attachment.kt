package com.yoavst.mashov.model

import com.google.gson.annotations.SerializedName

data class Attachment(@SerializedName("fileId") val id: String,@SerializedName("fileName") val name: String)