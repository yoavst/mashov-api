package com.yoavst.mashov.model

data class MessageTitle(val messageId: String, val subject: String, val senderName: String, val sendDate: Long, val isNew: Boolean, val hasAttachment: Boolean)