package com.yoavst.mashov.model

data class Message(val messageId: String, val sendDate: Long, val subject: String, val body: String, val sender: String, val attachments: List<Attachment>)