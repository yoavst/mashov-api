package com.yoavst.mashov.model

data class LessonCount(val groupId: Int, val lessonsCount: Int, val weeklyHours: Int)