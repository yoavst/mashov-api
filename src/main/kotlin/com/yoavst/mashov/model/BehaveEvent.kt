package com.yoavst.mashov.model

data class BehaveEvent(val groupId: Int, val lesson: Int, val date: Long, val type: Int, val text: String, val justificationId: Int,
                              val justification: String, val reporter: String, val subject: String)