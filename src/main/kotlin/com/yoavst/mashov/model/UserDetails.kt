package com.yoavst.mashov.model

import com.google.gson.annotations.SerializedName

data class UserDetails(val email: String, @SerializedName("cellphone") val phone: String)