package com.yoavst.mashov

import com.google.gson.*
import com.google.gson.reflect.TypeToken
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter
import java.io.IOException
import java.lang.reflect.Type
import java.util.*

inline fun <reified T : Any> Gson.fromJson(json: JsonReader): T = this.fromJson(json, typeToken<T>())

inline fun <reified T : Any> Gson.fromJson(json: JsonElement): T = this.fromJson(json, typeToken<T>())

inline fun <reified T : Any> typeToken(): Type = object : TypeToken<T>() {}.type

inline fun <reified T : Any> GsonBuilder.simpleDeserialize(noinline deserializer: (json: JsonElement) -> T?): GsonBuilder
        = this.registerTypeAdapter(typeToken<T>(), simpleJsonDeserializer(deserializer))

fun <T : Any> simpleJsonDeserializer(deserializer: (json: JsonElement) -> T?): JsonDeserializer<T>
        = JsonDeserializer<T> { json, type, context -> deserializer(json) }

private inline fun <T : Any> JsonElement?._nullOr(getNotNull: JsonElement.() -> T): T?
        = if (this == null || isJsonNull) null else getNotNull()

internal val JsonElement.string: String get() = asString
internal val JsonElement?.nullString: String? get() = _nullOr { string }
internal val JsonElement.bool: Boolean get() = asBoolean
internal val JsonElement.int: Int get() = asInt
internal val JsonElement?.nullInt: Int? get() = _nullOr { int }
internal val JsonElement.array: JsonArray get() = asJsonArray
internal val JsonElement.obj: JsonObject get() = asJsonObject

internal operator fun JsonElement.get(key: String): JsonElement {
    val value = obj.get(key)
    if (value != null) return value
    if (key == "grade") {
        obj.addProperty("grade", 0)
        return obj.get(key)
    }
    if (key == "files") {
        return JsonNull.INSTANCE
    }
    if (key == "messageId") {
        val cId = obj.get("conversationId")
        if (cId != null) return cId
    }
    if (key == "senderName") {
        val name = obj.get("messages").array.get(0).obj.get("senderName")
        if (name != null) return name
    }
    throw NoSuchElementException("no key $key in $obj")

}
internal operator fun JsonElement.get(index: Int): JsonElement = array.get(index)

internal operator fun JsonObject.contains(key: String): Boolean = has(key)


class NullStringToEmptyAdapterFactory<T> : TypeAdapterFactory {
    @Suppress("UNCHECKED_CAST", "CAST_NEVER_SUCCEEDS")
    override fun <T> create(gson: Gson, type: TypeToken<T>): TypeAdapter<T>? {

        val rawType = type.rawType as Class<T>
        if (rawType != String::class.java) {
            return null
        }
        return StringAdapter() as TypeAdapter<T>
    }
}

class StringAdapter : TypeAdapter<String>() {
    @Throws(IOException::class)
    override fun read(reader: JsonReader): String {
        if (reader.peek() == JsonToken.NULL) {
            reader.nextNull()
            return ""
        }
        return reader.nextString()
    }

    @Throws(IOException::class)
    override fun write(writer: JsonWriter, value: String?) {
        if (value == null) {
            writer.nullValue()
            return
        }
        writer.value(value)
    }
}
