package com.yoavst.mashov

import nl.komponents.kovenant.Promise

inline infix fun <K : Any> Promise<K?, Exception>.successNotNull(crossinline callback: (K) -> Unit) = success { if (it != null) callback(it) }