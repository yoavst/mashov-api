package com.yoavst.mashov.controller

import com.yoavst.mashov.controller.ApiController.Api
import com.yoavst.mashov.model.*
import nl.komponents.kovenant.*
import java.io.File
import java.net.URI
import java.net.URL
import java.util.*

class ApiControllerImpl(private val requestsController: RequestsController, private val cookieManager: CookieManager, private val dataParser: DataParser) : ApiController {
    override val ApiVersion: String = "3.20180928"

    //region Api Version Processing
    var apiProcessor: ((String) -> Boolean)? = null

    override fun attachApiVersionProcessor(processor: (String) -> Boolean) {
        apiProcessor = processor
    }

    override fun detachApiVersionProcessor() {
        apiProcessor = null
    }
    //endregion

    //region Raw Data Processing
    var rawDataProcessor: ((String, Api) -> Boolean)? = null


    override fun attachRawDataProcessor(rawDataProcessor: (String, Api) -> Boolean) {
        this.rawDataProcessor = rawDataProcessor
    }

    override fun detachRawDataProcessor() {
        rawDataProcessor = null
    }
    //endregion

    override fun getSchools(): Promise<List<School>?, Exception> = requestsController.get(SchoolsUrl, JsonHeader).apiVersion().passToRaw(Api.Schools) { dataParser.schools(it) }

    override fun login(school: School, id: String, password: String, year: Int): Promise<Login?, Exception> {
        val body = """{
        "school":{"semel":${school.id},"name":"${school.name.replace("\"", "\\\"")}","years":${school.years.joinToString(",", "[", "]")},"top":true},
        "username":"$id",
        "year":$year,
        "password":"$password",
        "semel":${school.id},
        "appName":"com.mashov.main",
        "appVersion":$ApiVersion}"""
        return requestsController.post(LoginUrl, JsonHeader + loginHeader(), body).apiVersion().passToRaw(Api.Login) { dataParser.login(it) }
    }

    override fun getMessagesCount(sessionId: String): Promise<MessagesCount?, Exception> = auth(sessionId, MessagesCountUrl, Api.MessagesCount) { dataParser.messagesCount(it) }
    override fun getGrades(sessionId: String, userId: String): Promise<List<Grade>?, Exception> = auth(sessionId, gradesUrl(userId), Api.Grades) { dataParser.grades(it) }
    override fun getBagrutGrades(sessionId: String, userId: String): Promise<List<BagrutGrade>?, Exception> = auth(sessionId, bagrutGradesUrl(userId), Api.BagrutGrades) { dataParser.bagrutGrades(it) }
    override fun getBehaveEvents(sessionId: String, userId: String): Promise<List<BehaveEvent>?, Exception> = auth(sessionId, behaveUrl(userId), Api.BehaveEvents) { dataParser.behaveEvents(it) }
    override fun getGroups(sessionId: String, userId: String): Promise<List<Group>?, Exception> = auth(sessionId, groupsUrl(userId), Api.Groups) { dataParser.groups(it) }
    override fun getLessonsCount(sessionId: String, userId: String): Promise<List<LessonCount>?, Exception> = auth(sessionId, lessonsCountUrl(userId), Api.LessonsCount) { dataParser.lessonsCount(it) }
    override fun getTimetable(sessionId: String, userId: String): Promise<List<Lesson>?, Exception> = auth(sessionId, timetableUrl(userId), Api.Timetable) { dataParser.timetable(it) }
    override fun getAlfon(sessionId: String, userId: String): Promise<List<Contact>?, Exception> = auth(sessionId, alfonUrl(userId), Api.Alfon) { dataParser.alfon(it) }
    override fun getMessages(sessionId: String): Promise<List<MessageTitle>?, Exception> =
            getMessagesCount(sessionId).then { count ->
                val requestsCount = (count!!.allMessages) / 20
                val promises = ArrayList<Promise<List<MessageTitle>?, Exception>>()
                for (i in requestsCount downTo 0) {
                    promises.add(auth(sessionId, getMessagesSkip(i), Api.Messages) { dataParser.messages(it) })
                }
                all(promises).then { lists ->
                    emptyList<MessageTitle>().toMutableList().apply {
                        for (list in lists) {
                            addAll(list!!.toMutableList())
                        }
                    }.toList()
                }
            }.unwrap()

    override fun getDetails(sessionId: String): Promise<UserDetails?, Exception> = auth(sessionId, DetailsUrl, Api.Details) { dataParser.details(it) }
    override fun getMessage(sessionId: String, messageId: String): Promise<Message?, Exception> = auth(sessionId, messageUrl(messageId), Api.Message) { dataParser.message(it) }
    override fun getAttachment(sessionId: String, attachmentId: Int, name: String, file: File): Promise<File, Exception> {
        return requestsController.getBinary(attachmentUrl(attachmentId, name).urlEncode(), JsonHeader + authHeader()).apiVersion() then {
            file.parentFile?.mkdirs()
            file.createNewFile()
            file.writeBytes(it)
            file
        }
    }

    override fun logout(sessionId: String): Promise<Unit, Exception> = requestsController.get(LogoutUrl, JsonHeader + authHeader()) then { Unit }

    override fun getPicture(sessionId: String, userId: String, file: File): Promise<File, Exception> {
        return requestsController.getBinary(pictureUrl(userId), JsonHeader + authHeader()).apiVersion() then {
            file.parentFile?.mkdirs()
            file.createNewFile()
            file.writeBytes(it)
            file
        }
    }

    private inline fun <reified K : Any> auth(sessionId: String, url: String, api: Api, crossinline parsing: (String) -> K) =
            requestsController.get(url, JsonHeader + authHeader()).apiVersion().passToRaw(api, parsing)

    private inline fun <reified K : Any> Promise<String, Exception>.passToRaw(api: ApiController.Api, crossinline work: (String) -> K) =
            then { (rawDataProcessor?.invoke(it, api) ?: true) to it } then { if (it.first) work(it.second) else null }

    private inline fun <reified K : Any> Promise<HttpResult<K>, Exception>.apiVersion(): Promise<K, Exception> = thenApply {
        cookieManager.processHeaders(headers)
        val version = headers[ApiVersionHeader]?.first() ?: ""
        if (ApiVersion == version || apiProcessor?.invoke(version) == true)
            data
        else throw ApiLevelException(ApiVersion, version)
    }

    private fun authHeader() = mapOf(
            "Cookie" to "MashovSessionID=${cookieManager.mashovSessionId}; Csrf-Token=${cookieManager.csrfToken}; uniquId=${cookieManager.uniquId}",
            "X-Csrf-Token" to cookieManager.csrfToken)

    private fun loginHeader() = mapOf("X-Csrf-Token" to cookieManager.csrfToken)

    private fun String.urlEncode(): String {
        val url = URL(this)
        val uri = URI(url.protocol, url.userInfo, url.host, url.port, url.path, url.query, url.ref)
        return uri.toASCIIString()
    }

    companion object {
        internal const val ApiVersionHeader = "ApiVersion"
        internal const val SchoolsUrl = "https://svc.mashov.info/api/schools"
        internal const val LoginUrl = "https://svc.mashov.info/api/login"
        //internal const val MessagesUrl = "https://svc.mashov.info/api/mail/labels/0/messages"
        internal const val DetailsUrl = "https://svc.mashov.info/api/user/details"
        internal const val LogoutUrl = "https://svc.mashov.info/api/logout"
        internal const val MessagesCountUrl = "https://web.mashov.info/api/mail/counts"
        internal fun gradesUrl(userId: String) = "https://svc.mashov.info/api/students/$userId/grades"
        internal fun behaveUrl(userId: String) = "https://svc.mashov.info/api/students/$userId/behave"
        internal fun groupsUrl(userId: String) = "https://svc.mashov.info/api/students/$userId/groups"
        internal fun timetableUrl(userId: String) = "https://svc.mashov.info/api/students/$userId/timetable"
        internal fun alfonUrl(userId: String) = "https://svc.mashov.info/api/students/$userId/alfon"
        internal fun bagrutGradesUrl(userId: String) = "https://svc.mashov.info/api/students/$userId/bagrut/grades"
        internal fun lessonsCountUrl(userId: String) = "https://svc.mashov.info/api/students/$userId/lessonsCount"
        internal fun messageUrl(messageId: String) = "https://svc.mashov.info/api/mail/messages/$messageId"
        internal fun pictureUrl(userId: String) = "https://svc.mashov.info/api/user/$userId/picture"
        internal fun attachmentUrl(fileId: Int, name: String) = "https://svc.mashov.info/api/mail/messages/$fileId/files/$fileId/$name"
        internal fun getMessagesSkip(requestCount: Int) = "https://web.mashov.info/api/mail/inbox/conversations?skip=${requestCount * 20}"
        internal val JsonHeader = mapOf("Content-Type" to "application/json;charset=UTF-8")
    }
}