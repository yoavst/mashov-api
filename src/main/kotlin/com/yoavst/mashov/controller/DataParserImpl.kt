package com.yoavst.mashov.controller

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.stream.JsonReader
import com.yoavst.mashov.*
import com.yoavst.mashov.model.*
import java.text.SimpleDateFormat

object DataParserImpl : DataParser {
    val gson: Gson by lazy {
        GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                .registerTypeAdapterFactory(NullStringToEmptyAdapterFactory<String>())
                .simpleDeserialize { src ->
                    val credential = src["credential"].obj
                    val children = src["accessToken"]["children"]
                    Login(gson.fromJson<LoginData>(credential).copy(sessionId = src["sessionId"].string), gson.fromJson(children))
                }
                .simpleDeserialize { src ->
                    LoginData("", src["userId"].string, src["idNumber"].string, src["userType"].int,
                            src["schoolUserType"].int, src["semel"].int, src["year"].int, src["correlationId"].string)
                }
                .simpleDeserialize { src ->
                    Grade(src["teacherName"].nullString ?: "", src["groupId"].nullInt ?: 0, src["subjectName"].nullString ?: "", src["eventDate"].string.date(),
                            src["gradingEvent"].nullString ?: "", src["gradeTypeId"].nullInt ?: 0, src["grade"].nullInt ?: 0)
                }
                .simpleDeserialize { src ->
                    BehaveEvent(src["groupId"].int, src["lesson"].int, src["lessonDate"].string.date(), src["lessonType"].int,
                            src["achvaName"].string, src["justificationId"].int, src["justification"].string, src["reporter"].string, src["subject"].string)
                }
                .simpleDeserialize { src ->
                    val hourData = src["timeTable"].obj
                    val details = src["groupDetails"].obj
                    Lesson(hourData["groupId"].int, hourData["day"].int, hourData["lesson"].int, details["subjectName"].string, details["teacherName"].string)
                }
                .simpleDeserialize { src ->
                    MessageTitle(src["messageId"].string, src["subject"].string, src["senderName"].string, src["sendTime"].string.date(),
                            src["isNew"].bool, src["hasAttachments"].bool)
                }
                .simpleDeserialize { src ->
                    Message(src["messageId"].string, src["sendTime"].string.date(), src["subject"].string, src["body"].string,
                            src["senderName"].string, src["files"].let { if (it.isJsonNull) emptyList<Attachment>() else gson.fromJson<List<Attachment>>(it) })
                }.simpleDeserialize { src ->
                    MessagesCount(src["allMessages"].int, src["inboxMessages"].int, src["newMessages"].int, src["unreadMessages"].int)
                }
                .create()
    }

    override fun school(reader: JsonReader): School = gson.fromJson(reader)

    override fun schools(reader: JsonReader): List<School> = gson.fromJson(reader)

    override fun login(reader: JsonReader): Login = gson.fromJson(reader)

    override fun grades(reader: JsonReader): List<Grade> = gson.fromJson(reader)

    override fun bagrutGrades(reader: JsonReader): List<BagrutGrade> = gson.fromJson(reader)

    override fun behaveEvents(reader: JsonReader): List<BehaveEvent> = gson.fromJson(reader)

    override fun groups(reader: JsonReader): List<Group> = gson.fromJson(reader)

    override fun lessonsCount(reader: JsonReader): List<LessonCount> = gson.fromJson(reader)

    override fun timetable(reader: JsonReader): List<Lesson> = gson.fromJson(reader)

    override fun alfon(reader: JsonReader): List<Contact> = gson.fromJson(reader)

    override fun messages(reader: JsonReader): List<MessageTitle> = gson.fromJson(reader)

    override fun message(reader: JsonReader): Message = gson.fromJson(reader)

    override fun details(reader: JsonReader): UserDetails = gson.fromJson(reader)

    override fun messagesCount(reader: JsonReader): MessagesCount = gson.fromJson(reader)

    private fun String.date(): Long {
        val format = SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ss")
        return format.parse(this).time
        /*
        val cal = Calendar.getInstance().apply { clear() }
        cal.set(substring(0, 4).toInt(), substring(5, 7).toInt() - 1, substring(8, 10).toInt())
        cal.set(Calendar.HOUR_OF_DAY, substring(11, 13).toInt())
        cal.set(Calendar.MINUTE, substring(14, 16).toInt())
        cal.set(Calendar.SECOND, substring(17, 19).toInt())
        return cal.timeInMillis*/
    }
}