package com.yoavst.mashov.controller

import com.google.gson.stream.JsonReader
import com.yoavst.mashov.model.*
import java.io.StringReader

interface DataParser {
    fun school(data: String): School  = school(JsonReader(StringReader(data)))
    fun schools(data: String): List<School> = schools(JsonReader(StringReader(data)))
    fun login(data: String): Login = login(JsonReader(StringReader(data)))
    fun grades(data: String): List<Grade> = grades(JsonReader(StringReader(data)))
    fun bagrutGrades(data: String): List<BagrutGrade> = bagrutGrades(JsonReader(StringReader(data)))
    fun behaveEvents(data: String): List<BehaveEvent> = behaveEvents(JsonReader(StringReader(data)))
    fun groups(data: String): List<Group> = groups(JsonReader(StringReader(data)))
    fun lessonsCount(data: String): List<LessonCount> = lessonsCount(JsonReader(StringReader(data)))
    fun timetable(data: String): List<Lesson> = timetable(JsonReader(StringReader(data)))
    fun alfon(data: String): List<Contact> = alfon(JsonReader(StringReader(data)))
    fun messages(data: String): List<MessageTitle> = messages(JsonReader(StringReader(data)))
    fun message(data: String): Message = message(JsonReader(StringReader(data)))
    fun details(data: String): UserDetails = details(JsonReader(StringReader(data)))
    fun messagesCount(data: String): MessagesCount = messagesCount(JsonReader(StringReader(data)))

    fun school(reader: JsonReader): School
    fun schools(reader: JsonReader): List<School>
    fun login(reader: JsonReader): Login
    fun grades(reader: JsonReader): List<Grade>
    fun bagrutGrades(reader: JsonReader): List<BagrutGrade>
    fun behaveEvents(reader: JsonReader): List<BehaveEvent>
    fun groups(reader: JsonReader): List<Group>
    fun lessonsCount(reader: JsonReader): List<LessonCount>
    fun timetable(reader: JsonReader): List<Lesson>
    fun alfon(reader: JsonReader): List<Contact>
    fun messages(reader: JsonReader): List<MessageTitle>
    fun message(reader: JsonReader): Message
    fun details(reader: JsonReader): UserDetails
    fun messagesCount(reader: JsonReader): MessagesCount
}