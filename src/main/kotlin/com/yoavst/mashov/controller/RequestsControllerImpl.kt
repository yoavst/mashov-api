package com.yoavst.mashov.controller


import com.github.kittinunf.fuel.core.Request
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.httpPost
import com.yoavst.mashov.model.HttpResult
import nl.komponents.kovenant.*

object RequestsControllerImpl : RequestsController {
    override fun get(url: String, headers: Map<String, String>, body: String?): Promise<HttpResult<String>, Exception> {
        val request = url.httpGet().header(headers)
        if (body != null) request.body(body)
        return request.promise()
    }

    override fun post(url: String, headers: Map<String, String>, body: String?): Promise<HttpResult<String>, Exception> {
        val request = url.httpPost().header(headers)
        if (body != null) request.body(body)
        return request.promise()
    }

    override fun getBinary(url: String, headers: Map<String, String>, body: String?): Promise<HttpResult<ByteArray>, Exception> {
        val request = url.httpGet().header(headers)
        if (body != null) request.body(body)
        return request.binaryPromise()
    }

    override fun postBinary(url: String, headers: Map<String, String>, body: String?): Promise<HttpResult<ByteArray>, Exception> {
        val request = url.httpPost().header(headers)
        if (body != null) request.body(body)
        return request.binaryPromise()
    }

    private fun Request.promise(): Promise<HttpResult<String>, Exception> {
          return task { responseString() } thenApply {
              HttpResult(first.path, second.httpStatusCode, second.httpResponseHeaders, third.get())
          }
    }

    private fun Request.binaryPromise(): Promise<HttpResult<ByteArray>, Exception> {
        return task { response() } thenApply {
            HttpResult(first.path, second.httpStatusCode, second.httpResponseHeaders, third.get())
        }
    }
}
