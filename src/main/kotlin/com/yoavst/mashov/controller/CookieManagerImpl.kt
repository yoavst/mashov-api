package com.yoavst.mashov.controller

import java.net.HttpCookie

object CookieManagerImpl : CookieManager {
    private val listeners: MutableMap<Int, () -> Unit> = linkedMapOf()
    override fun attachListener(id: Int, listener: () -> Unit) {
        listeners[id] = listener
    }

    override fun detachListener(id: Int) {
        listeners.keys -= id
    }

    override fun processHeaders(headers: Map<String, List<String>>) {
        val cToken = headers["X-Csrf-Token"]
        if (cToken != null) {
            csrfToken = cToken[0]
        }
        headers["Set-Cookie"]?.flatMap { HttpCookie.parse(it) }?.filter { it.name == "MashovSessionID" }?.firstOrNull { !it.value.isNullOrEmpty() }?.let {
            mashovSessionId = it.value
        }
        headers["Set-Cookie"]?.flatMap { HttpCookie.parse(it) }?.filter { it.name == "uniquId" }?.firstOrNull { !it.value.isNullOrEmpty() }?.let {
            uniquId = it.value
        }
    }

    override var csrfToken: String = ""
        get() = field
        set(value) {
            field = value
            listeners.values.forEach { it() }
        }

    override var mashovSessionId: String = ""
        get() = field
        set(value) {
            field = value
            listeners.values.forEach { it() }
        }

    override var uniquId: String = ""
        get() = field
        set(value) {
            field = value
            listeners.values.forEach { it() }
        }
}