package com.yoavst.mashov.controller

interface CookieManager {
    fun processHeaders(headers: Map<String, List<String>>)
    var csrfToken: String
    var mashovSessionId: String
    var uniquId: String

    fun attachListener(id: Int, listener: () -> Unit)

    fun detachListener(id: Int)
}