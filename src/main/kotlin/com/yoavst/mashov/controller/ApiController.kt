package com.yoavst.mashov.controller

import com.yoavst.mashov.model.*
import nl.komponents.kovenant.Promise
import java.io.File

interface ApiController {
    val ApiVersion: String

    fun getSchools(): Promise<List<School>?, Exception>
    fun login(school: School, id: String, password: String, year: Int): Promise<Login?, Exception>
    fun getPicture(sessionId: String, userId: String, file: File): Promise<File, Exception>
    fun getGrades(sessionId: String, userId: String): Promise<List<Grade>?, Exception>
    fun getBagrutGrades(sessionId: String, userId: String): Promise<List<BagrutGrade>?, Exception>
    fun getBehaveEvents(sessionId: String, userId: String): Promise<List<BehaveEvent>?, Exception>
    fun getGroups(sessionId: String, userId: String): Promise<List<Group>?, Exception>
    fun getLessonsCount(sessionId: String, userId: String): Promise<List<LessonCount>?, Exception>
    fun getTimetable(sessionId: String, userId: String): Promise<List<Lesson>?, Exception>
    fun getAlfon(sessionId: String, userId: String): Promise<List<Contact>?, Exception>
    fun getMessages(sessionId: String): Promise<List<MessageTitle>?, Exception>
    fun getMessage(sessionId: String, messageId: String): Promise<Message?, Exception>
    fun getDetails(sessionId: String): Promise<UserDetails?, Exception>
    fun getAttachment(sessionId: String, attachmentId: Int, name: String, file: File): Promise<File, Exception>
    fun logout(sessionId: String): Promise<Unit, Exception>
    fun getMessagesCount(sessionId: String): Promise<MessagesCount?, Exception>
    /**
     * The callback should return true if it want the data to be parsed.
     */
    fun attachApiVersionProcessor(processor: (String) -> Boolean)

    fun detachApiVersionProcessor()

    /**
     * The callback should return true if it want the data to be parsed.
     */
    fun attachRawDataProcessor(rawDataProcessor: (String, Api) -> Boolean)

    fun detachRawDataProcessor()

    enum class Api {
        Schools, Login, UserInfo, Grades, BagrutGrades, BehaveEvents, Groups, LessonsCount, Timetable, Alfon, Messages, Message, Details, MessagesCount
    }
}
