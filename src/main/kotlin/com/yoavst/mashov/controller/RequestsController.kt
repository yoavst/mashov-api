package com.yoavst.mashov.controller

import com.yoavst.mashov.model.HttpResult
import nl.komponents.kovenant.Promise

interface RequestsController {
    fun get(url: String, headers: Map<String, String> = emptyMap(), body: String? = null): Promise<HttpResult<String>, Exception>
    fun post(url: String, headers: Map<String, String> = emptyMap(), body: String? = null): Promise<HttpResult<String>, Exception>

    fun getBinary(url: String, headers: Map<String, String> = emptyMap(), body: String? = null): Promise<HttpResult<ByteArray>, Exception>
    fun postBinary(url: String, headers: Map<String, String> = emptyMap(), body: String? = null): Promise<HttpResult<ByteArray>, Exception>
}