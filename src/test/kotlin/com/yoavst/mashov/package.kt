package com.yoavst.mashov

import java.io.File

fun String.getTestResource(): File = File("src/test/resources/$this")

fun String.getData() = getTestResource().readText()

inline fun <K> time(name: String, block: () -> K) : K {
    val start = System.currentTimeMillis()
    val data = block()
    println("$name :: ${System.currentTimeMillis() - start}ms")
    return data
}
