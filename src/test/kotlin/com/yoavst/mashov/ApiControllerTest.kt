package com.yoavst.mashov

import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.core.interceptors.loggingInterceptor
import com.yoavst.mashov.controller.ApiControllerImpl
import com.yoavst.mashov.controller.CookieManagerImpl
import com.yoavst.mashov.controller.DataParserImpl
import com.yoavst.mashov.controller.RequestsControllerImpl
import com.yoavst.mashov.model.Login
import com.yoavst.mashov.model.School
import nl.komponents.kovenant.Kovenant
import nl.komponents.kovenant.testMode
import org.junit.Before
import org.junit.Test
import kotlin.test.fail

class ApiControllerTest {
    val api = ApiControllerImpl(RequestsControllerImpl, CookieManagerImpl, DataParserImpl)
    lateinit var school: School
    lateinit var login: Login
    @Before
    fun setup() {
        Kovenant.testMode {
            it.printStackTrace()
            fail(it.message)
        }
        Fuel.testMode {

        }
        FuelManager.instance.addRequestInterceptor(loggingInterceptor())
    }

    fun getSchools() {
        api.getSchools() successNotNull { schools ->
            school = schools.first { it.id == SchoolId }
            println(school)
        } fail {
            it.printStackTrace()
            fail("getSchools() should not fail")
        }
    }

    fun login() {
        api.login(school, Id, Password, Year) successNotNull { result ->
            login = result
            println(login)
        } fail {
            it.printStackTrace()
            fail("login() should not fail")
        }
    }

    fun getGrades() {
        api.getGrades(login.data.sessionId, login.students[0].id) successNotNull { grades ->
            println(grades[0])
        } fail {
            it.printStackTrace()
            fail("getGrades() should not fail")
        }
    }

    fun getBehaveEvents() {
        api.getBehaveEvents(login.data.sessionId, login.students[0].id) successNotNull { events ->
            println(events[0])
        } fail {
            it.printStackTrace()
            fail("getBehaveEvents() should not fail")
        }
    }

    fun getAlfon() {
        api.getAlfon(login.data.sessionId, login.students[0].id) successNotNull { events ->
            println(events[0])
        } fail {
            it.printStackTrace()
            fail("getAlfon() should not fail")
        }
    }

    fun getMessages() {
        api.getMessages(login.data.sessionId) successNotNull { messages ->
            println(messages[1])
            api.getMessage(login.data.sessionId, messages[1].messageId) successNotNull ::println fail {
                it.printStackTrace()
                fail("GetMessage() should not fail")
            }
        } fail {
            it.printStackTrace()
            fail("getMessages() should not fail")
        }
    }

    @Test
    fun testAll() {
//        getSchools()
//        login()
//        getGrades()
//        getAlfon()
//        getBehaveEvents()
//        getMessages()
    }

    companion object {


        val Id: String by lazy { System.getenv("id") }
         val Password: String by lazy { System.getenv("password") }
         val SchoolId: Int by lazy { System.getenv("school").toInt() }
        val Year: Int by lazy { System.getenv("year").toInt() }
    }
}