package com.yoavst.mashov;


import com.yoavst.mashov.controller.*;
import com.yoavst.mashov.model.School;
import org.junit.Test;

import java.util.List;

public class JavaTest {
    @Test
    public void testApi() {
        ApiController apiController = new ApiControllerImpl(RequestsControllerImpl.INSTANCE, CookieManagerImpl.INSTANCE, DataParserImpl.INSTANCE);
        apiController.getSchools().success(new Callback<List<School>>() {
            @Override
            public void process(List<School> schools) {
                // Handle schools

            }
        }).fail(new Callback<Exception>() {
            @Override
            void process(Exception exception) {
                exception.printStackTrace();
            }
        });

    }
}
