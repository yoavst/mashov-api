package com.yoavst.mashov

import com.yoavst.mashov.controller.DataParserImpl
import org.junit.Test
import kotlin.test.assertEquals

class DataParserTest {
    val parser = DataParserImpl

    @Test
    fun parseSchools() {
        "schools.json".getData().let { data ->
            val schools = time("schools") { parser.schools(data) }
            assertEquals(708, schools.size)
        }
    }

    @Test
    fun parseLoginData() {
        "login.json".getData().let { data ->
            val login = time("login") { parser.login(data) }
            login.data.apply {
                assertEquals("123456789", id)
                assertEquals(2016, year)
                assertEquals(123456, schoolId)
            }
            assertEquals(1, login.students.size)
            login.students.first().apply {
                assertEquals("שטרנברג", familyName)
                assertEquals("יואב", privateName)
                assertEquals("יא", classCode)
                assertEquals(4, classNum)
            }

        }
    }

    @Test
    fun parseGrades() {
        "grades.json".getData().let { data ->
            val grades = time("grades") { parser.grades(data) }
            assertEquals(27, grades.size)
        }
    }


    @Test
    fun parseBagrutGrades() {
        "bagrut_grades.json".getData().let { data ->
            val grades = time("bagrut grades") { parser.bagrutGrades(data) }
            assertEquals(2, grades.size)
        }
    }

    @Test
    fun parseBehaveEvents() {
        "behave.json".getData().let { data ->
            val behaveEvents = time("behave events") { parser.behaveEvents(data) }
            assertEquals(45, behaveEvents.size)
        }
    }

    @Test
    fun parseGroups() {
        "groups.json".getData().let { data ->
            val groups = time("groups") { parser.groups(data) }
            assertEquals(13, groups.size)
        }
    }

    @Test
    fun parseLessonsCount() {
        "lessonsCount.json".getData().let { data ->
            val lessonsCount = time("lessons count") { parser.lessonsCount(data) }
            assertEquals(12, lessonsCount.size)
        }
    }

    @Test
    fun parseTimetable() {
        "timetable.json".getData().let { data ->
            val timetable = time("timetable") { parser.timetable(data) }
            assertEquals(53, timetable.size)
        }
    }

    @Test
    fun parseContacts() {
        "alfon.json".getData().let { data ->
            val groups = time("contacts") { parser.alfon(data) }
            assertEquals(1, groups.size)
            groups[0].apply {
                assertEquals("שטרנברג", familyName)
                assertEquals("יואב", privateName)
                assertEquals("יא", classCode)
                assertEquals(4, classNum)
            }
        }
    }

    @Test
    fun parseMessages() {
//        "messages.json".getData().let { data ->
//            val messages = time("messages") { parser.messages(data) }
//            assertEquals(114, messages.size)
//        }
    }

    @Test
    fun parseMessage() {
//        "message_attachment.json".getData().let { data ->
//            val message = time("message") { parser.message(data) }
//            assertEquals("8312", message.messageId)
//        }
    }

    @Test
    fun parseDetails() {
        "details.json".getData().let { data ->
            val details = time("details") { parser.details(data) }
            assertEquals("yoav.sternberg@gmail.com", details.email)
            assertEquals("STUDENT_MOBILE_PHONE_COMES_HERE", details.phone)
        }
    }


}